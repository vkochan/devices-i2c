;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2023 Vadym Kochan <vadim4j@gmail.com>
;; SPDX-License-Identifier: MIT
#!r6rs

(library (devices i2c)
  (export
    i2c-bus-id
    i2c-bus-name
    i2c-bus-type
    list-i2c-busses
    get-i2c-bus
    i2c-bus-funcs
    i2c-bus-has-any-funcs?
    i2c-bus-read-byte
    i2c-bus-write-byte
    i2c-bus-read-word
    i2c-bus-write-word
    i2c-bus-scan)
  (import (rnrs)
          (pffi)
          (devices i2c private compat)
          (only (srfi :152) string-split string-contains))

(define libi2c (open-shared-object "libi2c.so"))

(define I2C-FUNC-I2C              #x00000001)
(define I2C-FUNC-SMBUS-BYTE       #x00060000)
(define I2C-FUNC-SMBUS-BYTE-DATA  #x00180000)
(define I2C-FUNC-SMBUS-WORD-DATA  #x00600000)
(define I2C-FUNC-SMBUS-BLOCK-DATA #x03000000)

(define i2c-func-name/bits
'(
  (i2c             .  #x00000001)
  (addr-10bit      .  #x00000002)
  (mangle          .  #x00000004)
  (pec             .  #x00000008)
  (nostart         .  #x00000010)
  (slave           .  #x00000020)
  (blk-proc-call   .  #x00008000)
  (quick           .  #x00010000)
  (read-byte       .  #x00020000)
  (write-byte      .  #x00040000)
  (read-byte-data  .  #x00080000)
  (write-byte-data .  #x00100000)
  (read-word-data  .  #x00200000)
  (write-word-data .  #x00400000)
  (proc-call       .  #x00800000)
  (read-blk-data   .  #x01000000)
  (write-blk-data  .  #x02000000)
  (i2c-read-blk    .  #x04000000)
  (i2c-write-blk   .  #x08000000)
  (host-notify     .  #x10000000)
  (byte            .  #x00060000)
  (byte-data       .  #x00180000)
  (word-data       .  #x00600000)
  (block-data      .  #x03000000)
  (i2c-block       .  #x0c000000)
))

(define ffi-i2c-bus-open (foreign-procedure libi2c int open_i2c (pointer)))
(define ffi-i2c-bus-close (foreign-procedure libi2c void close_i2c (pointer)))
(define ffi-i2c-get-bus-funcs (foreign-procedure libi2c int get_i2c_bus_funcs (int pointer)))
(define ffi-i2c-select-addr (foreign-procedure libi2c int select_i2c_addr (int int int)))
(define ffi-i2c-read-byte (foreign-procedure libi2c int i2c_read_byte (int int pointer int)))
(define ffi-i2c-write-byte (foreign-procedure libi2c int i2c_write_byte (int int uint8_t int)))
(define ffi-i2c-read-word (foreign-procedure libi2c int i2c_read_word (int int pointer)))
(define ffi-i2c-write-word (foreign-procedure libi2c int i2c_write_word (int int uint32_t)))

(define i2c-busses #f)

(define-record-type i2c-bus
  (fields id name fd type func-bits))

(define (get-i2c-func-bits fd)
  (let ((bv (make-bytevector 4 0)))
      (let ((ret (ffi-i2c-get-bus-funcs fd (bytevector->pointer bv))))
        (when (< ret 0)
          (error 'get-i2c-func-bits "Could not funcs of the i2c bus"))
        (bytevector-u32-native-ref bv 0))))

(define (list-i2c-busses)
  (or i2c-busses
      (let ()
        (set! i2c-busses (list))
        (for-each
          (lambda (file)
            (let* ([id (string->number (cadr (string-split file "-")))]
                   [name
                    (call-with-input-file
                      (string-append "/sys/class/i2c-dev/" file "/device/name")
                      (lambda (p)
                        (get-line p)))]
                   [fd (ffi-i2c-bus-open (string-append "/dev/i2c-" (number->string id)))]
                   [func-bits (get-i2c-func-bits fd)]
                   [type
                    (cond
                      [(string-contains name "ISA") 'isa]
                      [else
                       (cond
                         [(positive? (bitwise-and func-bits I2C-FUNC-I2C))  'i2c]
                         [(positive? (bitwise-and func-bits (bitwise-ior
                                                              I2C-FUNC-SMBUS-BYTE
                                                              I2C-FUNC-SMBUS-BYTE-DATA
                                                              I2C-FUNC-SMBUS-WORD-DATA))) 'smbus]
                         [else 'dummy])])])
              (when (>= fd 0)
                (set! i2c-busses (append i2c-busses (list (make-i2c-bus id name fd type func-bits)))))))
          (directory-list "/sys/class/i2c-dev"))
        i2c-busses)))

(define (get-i2c-bus id)
  (let loop ([ls (list-i2c-busses)])
    (if (null? ls)
        #f
        (if (eqv? id (i2c-bus-id (car ls)))
            (car ls)
            (loop (cdr ls))))))

(define (i2c-bus-funcs bus)
  (let loop ((keys i2c-func-name/bits) (res '()))
    (if (null? keys)
        res
        (let* ((k (car keys))
               (name (car k))
               (mask (cdr k)))
          (if (positive? (bitwise-and mask (i2c-bus-func-bits bus)))
              (loop (cdr keys) (append res (list name)))
              (loop (cdr keys) res))))))

(define (i2c-bus-has-any-funcs? bus lst)
  (let loop ((lst lst) (funcs (i2c-bus-funcs bus)))
    (if (null? lst)
      #f
      (if (member (car lst) funcs)
        #t
        (loop (cdr lst) funcs)))))

(define (select-i2c-addr bus addr enforce?)
  (let ([ret (ffi-i2c-select-addr (i2c-bus-fd bus) addr (if enforce? 1 0))])
    (when (< ret 0)
      (error 'select-i2c-addr "Could not select i2c addr" bus addr))))

(define (i2c-bus-read-byte/data bus addr reg data?)
  (select-i2c-addr bus addr #t)
  (let ([bv (make-bytevector 1 0)])
      (let ([ret (ffi-i2c-read-byte (i2c-bus-fd bus) reg (bytevector->pointer bv) (if data? 1 0))])
        (when (< ret 0)
          (if data?
              (error 'i2c-bus-read-byte "Could not read byte data from the i2c bus" bus addr reg)
              (error 'i2c-bus-read-byte "Could not read byte from the i2c bus" bus addr)))
        (bytevector-u8-ref bv 0))))

(define i2c-bus-read-byte
  (case-lambda
    [(bus addr) (i2c-bus-read-byte/data bus addr 0 #f)]
    [(bus addr reg) (i2c-bus-read-byte/data bus addr reg #t)]))

(define (i2c-bus-write-byte/data bus addr reg val data?)
  (select-i2c-addr bus addr #t)
  (let ([ret (ffi-i2c-write-byte (i2c-bus-fd bus) reg val (if data? 1 0))])
    (when (< ret 0)
      (if data?
          (error 'i2c-bus-read-byte "Could not write byte data to the i2c bus" bus addr reg)
          (error 'i2c-bus-read-byte "Could not write byte to the i2c bus" bus addr)))))

(define i2c-bus-write-byte
  (case-lambda
    [(bus addr val) (i2c-bus-write-byte/data bus addr 0 val #f)]
    [(bus addr reg val) (i2c-bus-write-byte/data bus addr reg val #t)]))

(define (i2c-bus-read-word bus addr reg)
  (select-i2c-addr bus addr #t)
  (let ([bv (make-bytevector 2 0)])
      (let ([ret (ffi-i2c-read-word (i2c-bus-fd bus) reg (bytevector->pointer bv))])
        (when (< ret 0)
          (error 'i2c-bus-read-word "Could not read word data from the i2c bus" bus addr reg))
        (bytevector-u16-native-ref bv 0))))

(define (i2c-bus-write-word bus addr reg val)
  (select-i2c-addr bus addr #t)
  (let ([ret (ffi-i2c-write-word (i2c-bus-fd bus) reg val)])
    (when (< ret 0)
      (error 'i2c-bus-write-word "Could not write word data to the i2c bus" bus addr reg))))

(define (i2c-bus-scan bus)
  (when (i2c-bus-has-any-funcs? bus '(read-byte))
    (let loop ((addr 0) (lst '()))
      (if (< addr 128)
          (if (guard (e [(error? e) #f]) (i2c-bus-read-byte bus addr))
              (loop (+ addr 1) (append lst (list addr)))
              (loop (+ addr 1) lst))
          lst))))

)
