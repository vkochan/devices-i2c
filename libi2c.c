#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <inttypes.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#define I2C_F_UNKNOWN -1
#define I2C_F_DUMMY    0
#define I2C_F_I2C      1
#define I2C_F_SMBUS    2

int open_i2c(const char *path)
{
    int fd = open(path, O_RDWR);
    if (fd < 0)
        return -errno;
    return fd;
};

int get_i2c_bus_funcs(int fd, uint32_t *funcs)
{
    unsigned long val;
    int err;

    if (fd < 0)
        return -1;

    err = ioctl(fd, I2C_FUNCS, &val);
    if (err < 0)
        return -errno;
    *funcs = (uint32_t)val;
    return 0;
}

int select_i2c_addr(int fd, int addr, int enforce)
{
    int ret = ioctl(fd, enforce ? I2C_SLAVE_FORCE: I2C_SLAVE, addr);
    if (ret < 0)
        return -errno;
    return 0;
};

static int i2c_read_write_request(int fd, int rw, uint8_t req,
		                  union i2c_smbus_data *data,
				  int size)
{
    struct i2c_smbus_ioctl_data ioc;
    int ret;

    ioc.data = data;
    ioc.size = size;
    ioc.command = req;
    ioc.read_write = rw;

    ret = ioctl(fd, I2C_SMBUS, &ioc);
    if (ret == -1)
        return -errno;
    return ret;
};

int i2c_read_byte(int fd, int reg, uint8_t *val, int is_data)
{
    union i2c_smbus_data data;
    int ret;

    ret = i2c_read_write_request(fd, I2C_SMBUS_READ, is_data ? reg : 0, &data,
		                 is_data ? I2C_SMBUS_BYTE_DATA : I2C_SMBUS_BYTE);
    if (ret < 0) {
        return -errno;
    };
    *val = data.byte;
    return 0;
};

int i2c_write_byte(int fd, int reg, uint8_t val, int is_data, int is_quick)
{
    if (is_data) {
        union i2c_smbus_data data;
	data.byte = val;
        return i2c_read_write_request(fd, I2C_SMBUS_WRITE, reg, &data, I2C_SMBUS_BYTE_DATA);
    } else {
        return i2c_read_write_request(fd, I2C_SMBUS_WRITE, val, NULL, I2C_SMBUS_BYTE);
    }
};

int i2c_read_word(int fd, int reg, uint16_t *val)
{
    union i2c_smbus_data data;
    int ret;

    ret = i2c_read_write_request(fd, I2C_SMBUS_READ, reg, &data, I2C_SMBUS_WORD_DATA);
    if (ret < 0)
        return -errno;
    *val = data.word;
    return 0;
};

int i2c_write_word(int fd, int reg, uint16_t val)
{
    union i2c_smbus_data data;
    int ret;

    data.word = val;
    ret = i2c_read_write_request(fd, I2C_SMBUS_WRITE, reg, &data, I2C_SMBUS_WORD_DATA);
    if (ret < 0)
        return -errno;
    return 0;
};

void close_i2c(int fd)
{
    close(fd);
};
