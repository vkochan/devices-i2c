#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright (c) 2023 
;; SPDX-License-Identifier: MIT
#!r6rs

(import (rnrs (6)) (srfi :64 testing) (devices i2c))

(test-begin "list i2c-busses")
;;(test-equal (list) (list-i2c-busses))
(display (list-i2c-busses))
(test-end)

(exit (if (zero? (test-runner-fail-count (test-runner-get))) 0 1))
