#!r6rs

(library (devices i2c private compat)
  (export
    directory-list)
  (import
    (only (chezscheme) directory-list))
)
